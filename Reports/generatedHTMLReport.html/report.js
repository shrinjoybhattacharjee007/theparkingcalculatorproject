$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ParkingCalc.feature");
formatter.feature({
  "line": 2,
  "name": "Calculate different parking fees",
  "description": "",
  "id": "calculate-different-parking-fees",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Parking"
    }
  ]
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Calculate parkinf fee for \"\u003cTestCase\u003e\"",
  "description": "",
  "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\"",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "The user Opened the AUT.",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "the user puts the data for \"\u003cTestCase\u003e\".",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "submits the input data.",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "validate the outcome for \"\u003cTestCase\u003e\".",
  "keyword": "Then "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\";",
  "rows": [
    {
      "cells": [
        "TestCase"
      ],
      "line": 12,
      "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\";;1"
    },
    {
      "cells": [
        "TC001"
      ],
      "line": 13,
      "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\";;2"
    },
    {
      "cells": [
        "TC002"
      ],
      "line": 14,
      "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\";;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 158133600,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Calculate parkinf fee for \"TC001\"",
  "description": "",
  "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\";;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@Parking"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "The user Opened the AUT.",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "the user puts the data for \"TC001\".",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "submits the input data.",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "validate the outcome for \"TC001\".",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefinitions.the_user_Opened_to_AUT()"
});
formatter.write("\u003ca href\u003d\"E:\\K2Range\\benu\\theparkingcalculatorproject\\Reports\\GeneratdScreenShots\\Sandbox\\2021-05-12\\Sandbox_2021-05-12_1620764859591.jpg\" target\u003d\"_blank\"\u003eScreenShot\u003c/a\u003e ");
formatter.result({
  "duration": 4757549700,
  "status": "passed"
});
formatter.match({
  "location": "elementOperation.the_user_waits_for_the_page_to_load()"
});
formatter.result({
  "duration": 5002929600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC001",
      "offset": 28
    }
  ],
  "location": "SFSteps.the_user_puts_the_data_for(String)"
});
formatter.result({
  "duration": 1050653500,
  "status": "passed"
});
formatter.match({
  "location": "elementOperation.the_user_waits_for_the_page_to_load()"
});
formatter.result({
  "duration": 5011682800,
  "status": "passed"
});
formatter.match({
  "location": "SFSteps.submits_the_input_data()"
});
formatter.result({
  "duration": 425303400,
  "status": "passed"
});
formatter.match({
  "location": "elementOperation.the_user_waits_for_the_page_to_load()"
});
formatter.result({
  "duration": 5001083700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC001",
      "offset": 26
    }
  ],
  "location": "SFSteps.validate_the_outcome_for(String)"
});
formatter.result({
  "duration": 81025500,
  "status": "passed"
});
formatter.after({
  "duration": 890488600,
  "status": "passed"
});
formatter.before({
  "duration": 21300,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Calculate parkinf fee for \"TC002\"",
  "description": "",
  "id": "calculate-different-parking-fees;calculate-parkinf-fee-for-\"\u003ctestcase\u003e\";;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@Parking"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "The user Opened the AUT.",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "the user puts the data for \"TC002\".",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "submits the input data.",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "the user waits for the page to load.",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "validate the outcome for \"TC002\".",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "stepDefinitions.the_user_Opened_to_AUT()"
});
formatter.write("\u003ca href\u003d\"E:\\K2Range\\benu\\theparkingcalculatorproject\\Reports\\GeneratdScreenShots\\Sandbox\\2021-05-12\\Sandbox_2021-05-12_1620764881024.jpg\" target\u003d\"_blank\"\u003eScreenShot\u003c/a\u003e ");
formatter.result({
  "duration": 3957486000,
  "status": "passed"
});
formatter.match({
  "location": "elementOperation.the_user_waits_for_the_page_to_load()"
});
formatter.result({
  "duration": 5004853000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC002",
      "offset": 28
    }
  ],
  "location": "SFSteps.the_user_puts_the_data_for(String)"
});
formatter.result({
  "duration": 956446600,
  "status": "passed"
});
formatter.match({
  "location": "elementOperation.the_user_waits_for_the_page_to_load()"
});
formatter.result({
  "duration": 5009929900,
  "status": "passed"
});
formatter.match({
  "location": "SFSteps.submits_the_input_data()"
});
formatter.result({
  "duration": 409023300,
  "status": "passed"
});
formatter.match({
  "location": "elementOperation.the_user_waits_for_the_page_to_load()"
});
formatter.result({
  "duration": 5008439200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TC002",
      "offset": 26
    }
  ],
  "location": "SFSteps.validate_the_outcome_for(String)"
});
formatter.result({
  "duration": 75272300,
  "status": "passed"
});
formatter.after({
  "duration": 1182205700,
  "status": "passed"
});
});